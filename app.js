const express = require('express');
const passport = require('passport');
const session = require("express-session");
const bodyParser = require("body-parser");
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20');
const FacebookStrategy = require('passport-facebook').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
require('dotenv').config();
const app = express();

const ClientID = process.env.googleKey;
const  ClientSecret = process.env.googleSecret;

const facebookAppId = process.env.facebookKey;
const facebookSecret = process.env.facebookSecret;

const twitterAppKey = process.env.twitterKey;
const twitterAppSecret = process.env.twitterSecret;

app.use(express.static("public"));
app.use(session({ secret: "cats", resave: false, saveUninitialized: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());



passport.use(new TwitterStrategy({
    consumerKey: twitterAppKey,
    consumerSecret: twitterAppSecret,
    callbackURL: "http://localhost:3000/auth/twitter/callback"
  },
  function(token, tokenSecret, profile, done) {
    console.log(profile);
   done(null, {user: "Dhanush"});
  }
));


passport.use(new GoogleStrategy({
    clientID: ClientID,
    clientSecret: ClientSecret,
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
      console.log(profile);
   return cb(null, {user: 'Dhanush'});
  }
));


passport.use(new FacebookStrategy({
  clientID: facebookAppId,
  clientSecret: facebookSecret,
  callbackURL: "http://localhost:3000/auth/facebook/callback"
},
function(accessToken, refreshToken, profile, done) {
 console.log(profile);
  done(null,{name: "Dhanush"})
}
));



app.get('/',(req,res)=>{

res.send('Welcome');

});

app.get('/login',(req,res)=>{
    res.send('Login First');
})


app.get('/auth/google',
  passport.authenticate('google', {scope: ['profile'], session: false}
  
  ));

app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login', session: false }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

  app.get('/auth/facebook', passport.authenticate('facebook'));

 
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
                                        failureRedirect: '/login' }));


app.get('/auth/twitter', passport.authenticate('twitter', {session: false}));


app.get('/auth/twitter/callback',
  passport.authenticate('twitter', { successRedirect: '/',
                                     failureRedirect: '/login', session: false }));




app.listen(3000);